    const_def 2 ; object constants
    const STARTERPACK_PICKUP

RedsHouse2F_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .InitializeRoom
	; callback MAPCALLBACK_TILES, .SetSpawn

; unused
.Null:
	end

.InitializeRoom:
	setevent EVENT_TEMPORARY_UNTIL_MAP_RELOAD_8
	checkevent EVENT_INITIALIZED_EVENTS
	iftrue .SkipInitialization
	jumpstd initializeevents
	return

.SkipInitialization:
	return

.SetSpawn:
	special ToggleMaptileDecorations
	return

RedsHouseBedScript:
	opentext
	writetext RedsHouseBedText
	yesorno
	iffalse .EndBedScript
	closetext
	special FadeBlackQuickly
	special ReloadSpritesNoPalettes
	special StubbedTrainerRankings_Healings
	playmusic MUSIC_HEAL
	special HealParty
	pause 60
	special FadeInQuickly
	special RestartMapMusic
	end
.EndBedScript
	closetext
	end

RedsHousePCScript:
	opentext
	special PlayersHousePC
	iftrue .Warp
	closetext
	end
.Warp:
	warp NONE, 0, 0
	end

RedsHouseEngineScript:
    opentext
    writetext RedsHouseEngineText
    setflag ENGINE_POKEGEAR
    setflag ENGINE_MAP_CARD
    setflag ENGINE_PHONE_CARD
    setflag ENGINE_RADIO_CARD
    setflag ENGINE_EXPN_CARD
    setflag ENGINE_POKEDEX
    setevent EVENT_GOT_POKEGEAR
    disappear STARTERPACK_PICKUP
    waitbutton
    closetext
    end

RedsHouse2FPCScript:
	opentext
	writetext RedsHouse2FPCText
	givepoke MEW, 100, LEFTOVERS
	closetext
	end

RedsHouseEngineText:
    text "Can't leave without"
    line "my stuff."

    para "Got #DEX and"
    line "#GEAR."
    done

RedsHouseBedText:
	text "Would you like to"
	line "rest a moment?"
	done

RedsHouse2FPCText:
	text "It looks like it"
	line "hasn't been used"
	cont "in a long time…"
	done

RedsHouse2F_MapEvents:
	db 0, 0 ; filler

	db 1 ; warp events
	warp_event  7,  0, REDS_HOUSE_1F, 3

	db 0 ; coord events

	db 3 ; bg events
	bg_event  0,  1, BGEVENT_READ, RedsHousePCScript
	bg_event  0,  6, BGEVENT_READ, RedsHouseBedScript
	bg_event  0,  7, BGEVENT_READ, RedsHouseBedScript

	db 1 ; object events
    object_event  7,  6, SPRITE_POKEDEX, SPRITEMOVEDATA_STILL, 0, 0, -1, -1, PAL_NPC_RED, OBJECTTYPE_SCRIPT, 0, RedsHouseEngineScript, EVENT_GOT_POKEGEAR
