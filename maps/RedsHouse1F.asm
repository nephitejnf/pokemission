	const_def 2 ; object constants

RedsHouse1F_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

PlayerCompanionScript:
	jumptextfaceplayer PlayerCompanionText

RedsHouse1FBookshelf:
	jumpstd picturebookshelf

PlayerCompanionText:
	text "Good morning!"
	line "How are you"
	cont "today, <PLAYER>?"
	done

RedsHouse1F_MapEvents:
	db 0, 0 ; filler

	db 3 ; warp events
	warp_event  2,  7, PALLET_TOWN, 1
	warp_event  3,  7, PALLET_TOWN, 1
	warp_event  7,  0, REDS_HOUSE_2F, 1

	db 0 ; coord events

	db 2 ; bg events
	bg_event  4,  1, BGEVENT_READ, RedsHouse1FBookshelf
	bg_event  5,  1, BGEVENT_READ, RedsHouse1FBookshelf

	db 1 ; object events
	object_event  6,  6, SPRITE_SUPER_NERD, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, PAL_NPC_BROWN, OBJECTTYPE_SCRIPT, 0, PlayerCompanionScript, -1
