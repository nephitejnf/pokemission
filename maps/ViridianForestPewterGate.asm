    const_def 2
    const VIRIDIANFOREST_PEWTER_GATE_OFFICER

ViridianForestPewterGate_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

ViridianForestPewterGateOfficerScript:
    jumptextfaceplayer ViridianForestPewterGateOfficerText

ViridianForestPewterGateOfficerText:
	text "Are you headed"
	line "to Pewter City?"

	para "Be sure to check"
	line "out the Museum!"
	done

ViridianForestPewterGate_MapEvents:
    db 0, 0

	db 4 ; warp events
	warp_event  4,  7, VIRIDIAN_FOREST, 2
	warp_event  5,  7, VIRIDIAN_FOREST, 1
	warp_event  4,  0, ROUTE_2, 7
	warp_event  5,  0, ROUTE_2, 8

	db 0 ; coord events

	db 0 ; bg events

	db 1 ; object events
	object_event  0,  4, SPRITE_OFFICER, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, PAL_NPC_BLUE, OBJECTTYPE_SCRIPT, 0, ViridianForestPewterGateOfficerScript, -1

