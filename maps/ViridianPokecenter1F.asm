	const_def 2 ; object constants
	const VIRIDIANPOKECENTER1F_NURSE
	const VIRIDIANPOKECENTER1F_COOLTRAINER_M
	const VIRIDIANPOKECENTER1F_COOLTRAINER_F
	const VIRIDIANPOKECENTER1F_BUG_CATCHER

ViridianPokecenter1F_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

ViridianPokecenter1FNurseScript:
	jumpstd pokecenternurse

ViridianPokecenter1FCooltrainerMScript:
	faceplayer
	opentext
	checkevent EVENT_BLUE_IN_CINNABAR
	iftrue .BlueReturned
	writetext ViridianPokecenter1FCooltrainerMText
	waitbutton
	closetext
	end

.BlueReturned:
	writetext ViridianPokecenter1FCooltrainerMText_BlueReturned
	waitbutton
	closetext
	end

ViridianPokecenter1FCooltrainerFScript:
	faceplayer
    opentext
    checkevent EVENT_GOT_COOLTRAINER_POTION_GIVEN
    iftrue .PotionComplete
    checkevent EVENT_GOT_COOLTRAINER_POTION_ASK
    iftrue .PotionAsking
    writetext ViridianPokecenter1FCooltrainerFText
    yesorno
    iftrue .PotionYesMan
    writetext ViridianPokecenter1FCooltrainerFDeclineText
    jump .done
.PotionYesMan:
    writetext ViridianPokecenter1FCooltrainerFAcceptText
    setevent EVENT_GOT_COOLTRAINER_POTION_ASK
    jump .done
.PotionAsking:
    checkitem POTION
    iftrue .PotionGiving
    writetext ViridianPokecenter1FCooltrainerFNoPotionText
    jump .done
.PotionGiving:
    takeitem POTION, 1
    writetext ViridianPokecenter1FCooltrainerFPotionGetText
    setevent EVENT_GOT_COOLTRAINER_POTION_GIVEN
    jump .done
.PotionComplete:
    writetext ViridianPokecenter1FCooltrainerFCompleteText

.done
    waitbutton
    closetext
    end

ViridianPokecenter1FBugCatcherScript:
	jumptextfaceplayer ViridianPokecenter1FBugCatcherText

ViridianPokecenter1FCooltrainerMText:
	text "Where in the world"
	line "is VIRIDIAN's GYM"

	para "LEADER? I wanted"
	line "to challenge him."
	done

ViridianPokecenter1FCooltrainerMText_BlueReturned:
	text "There are no GYM"
	line "TRAINERS at the"
	cont "VIRIDIAN GYM."

	para "The LEADER claims"
	line "his policy is to"

	para "win without having"
	line "any underlings."
	done

ViridianPokecenter1FCooltrainerFText:
	text "I wish someone"
    line "could help me"
    cont "with an errand."

    para "Do you have"
    line "time to help?"
	done

ViridianPokecenter1FCooltrainerFAcceptText:
    text "Awesome! You're"
    line "the best!"

    para "I just need a"
    line "POTION from the"
    cont "MART."

    para "The Nurse says she"
    line "needs one to heal"
    cont "my #MON."
    done

ViridianPokecenter1FCooltrainerFDeclineText:
    text "Awww! Let me know"
    line "if you change"
    cont "your mind."
    done

ViridianPokecenter1FCooltrainerFNoPotionText:
    text "Did you get that"
    line "POTION, yet?"
    cont "I need it!"
    done

ViridianPokecenter1FCooltrainerFPotionGetText:
    text "Awwwww!"
    line "You got it!"
    cont "Thank you!"
    cont "Thank you so much!"
    done

ViridianPokecenter1FCooltrainerFCompleteText:
    text "Thanks again! This"
    line "town is so nice!"
    done

ViridianPokecenter1FBugCatcherText:
	text "My dream is to be-"
	line "come a GYM LEADER."
	done

ViridianPokecenter1F_MapEvents:
	db 0, 0 ; filler

	db 3 ; warp events
	warp_event  3,  7, VIRIDIAN_CITY, 5
	warp_event  4,  7, VIRIDIAN_CITY, 5
	warp_event  0,  7, POKECENTER_2F, 1

	db 0 ; coord events

	db 0 ; bg events

	db 4 ; object events
	object_event  3,  1, SPRITE_NURSE, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ViridianPokecenter1FNurseScript, -1
	object_event  8,  4, SPRITE_COOLTRAINER_M, SPRITEMOVEDATA_WALK_LEFT_RIGHT, 1, 0, -1, -1, PAL_NPC_RED, OBJECTTYPE_SCRIPT, 0, ViridianPokecenter1FCooltrainerMScript, -1
	object_event  5,  3, SPRITE_COOLTRAINER_F, SPRITEMOVEDATA_STANDING_UP, 0, 0, -1, -1, PAL_NPC_BLUE, OBJECTTYPE_SCRIPT, 0, ViridianPokecenter1FCooltrainerFScript, -1
	object_event  1,  6, SPRITE_BUG_CATCHER, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, PAL_NPC_GREEN, OBJECTTYPE_SCRIPT, 0, ViridianPokecenter1FBugCatcherScript, -1
