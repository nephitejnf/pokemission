    const_def 2
    const VIRIDIANFOREST_ITEMBALL1
    const VIRIDIANFOREST_ITEMBALL2

ViridianForest_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

ViridianForestSignScript1:
    jumptext ViridianForestSignText1

ViridianForestSignScript2:
    jumptext ViridianForestSignText2

ViridianForestSignScript3:
    jumptext ViridianForestSignText3

ViridianForestSignScript4:
    jumptext ViridianForestSignText4

ViridianForestSignScript5:
    jumptext ViridianForestSignText5

ViridianForestSignScript6:
    jumptext ViridianForestSignText6

ViridianForestSignText1:
	text "Leaving"
	line "Viridian Forest"
	cont "Pewter City Ahead"
	done

ViridianForestSignText2:
	text "Trainer Tips"

	para "Hold on to that"
	line "Big Mushroom!"

	para "Some maniacs will"
	line "pay lots of money"
	cont "for useless items!"
	done

ViridianForestSignText3:
	text "Trainer Tips"

	para "Grass-type #mon"
	line "are unaffected by"

	para "powder and spore"
	line "moves!"
	done

ViridianForestSignText4:
	text "For poison, use"
	line "Antidote! Get it"
	cont "at #mon Marts!"
	done

ViridianForestSignText5:
	text "Trainer Tips"

	para "Poison-type #-"
	line "mon can't be poi-"
	cont "soned themselves!"
	done

ViridianForestSignText6:
	text "Trainer Tips"

	para "Weaken #mon"
	line "before attempting"
	cont "capture!"

	para "When healthy,"
	line "they may escape!"
	done

ViridianForest_MapEvents:
    db 0, 0

	db 4 ; warp events
	warp_event  2,  0, VIRIDIAN_FOREST_PEWTER_GATE, 2
	warp_event  1,  0, VIRIDIAN_FOREST_PEWTER_GATE, 1
	warp_event 16, 47, VIRIDIAN_FOREST_VIRIDIAN_GATE, 1
	warp_event 17, 47, VIRIDIAN_FOREST_VIRIDIAN_GATE, 2

	db 0 ; coord events

	db 6 ; bg events
	bg_event  2,  2, BGEVENT_READ, ViridianForestSignScript1
	bg_event  5, 25, BGEVENT_READ, ViridianForestSignScript2
	bg_event 26, 16, BGEVENT_READ, ViridianForestSignScript3
	bg_event 16, 32, BGEVENT_READ, ViridianForestSignScript4
	bg_event 24, 40, BGEVENT_READ, ViridianForestSignScript5
	bg_event 19, 43, BGEVENT_READ, ViridianForestSignScript6

	db 2 ; object events
	itemball 14, 31, DIRE_HIT, 1, EVENT_ROUTE_2_DIRE_HIT
	itemball  3, 33, MAX_POTION, 1, EVENT_ROUTE_2_MAX_POTION
